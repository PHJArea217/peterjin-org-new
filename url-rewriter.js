'use strict';
/* ALWAYS MAKE SURE WE USE STUFF COMPATIBLE WITH EVEN VERY OLD BROWSERS */
function _peterjin_org_rewrite_urls(params) {
	var reg = new RegExp("^https?://[^/]*/");
	if (params.url_rewrite) {
		var url = params.url_rewrite;
		var allElems = document.getElementsByTagName("a");
		if (allElems) {
			for (var i = 0; i < allElems.length; i++) {
				var elem = allElems.item(i);
				var rwc = String(elem.getAttribute("rewrite-class"));
				if (rwc = url[rwc]) { /* The = is not a typo */
					var origHref = elem.getAttribute("href");
					if (origHref) elem.setAttribute("href", origHref.replace(reg, rwc));
				}
			}
		}
	}
}
function _rewrite_test() {
	_peterjin_org_rewrite_urls({url_rewrite: {}});
}
function _peterjin_org_add_scripts_css() {
	var urls = ["https://"];
}