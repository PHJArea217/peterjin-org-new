#!/bin/sh

mkdir -p public/misc
cp favicon.ico public/
cp -r misc public/

for x in about-me.html:https://about.peterjin.org gtw.html:https://webapps.peterjin.org/GuessTheWord.html sitemap.html:https://about.peterjin.org/website.html; do
	cat > "public/${x%%:*}" <<EOF
<html><head><script>window.location.href="${x#*:}";</script></head><body>MOVED: <a href="${x#*:}">${x#*:}</a>Please update your bookmarks.</body></html>
EOF
done
