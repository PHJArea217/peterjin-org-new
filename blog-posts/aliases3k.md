2019-03-24|8|-|-|Aliases 3000

Aliases 3000 is a system that allows me to effectively use aliases or
other names with an intent to prefer correspondence in email or other
settings.

# Rationale / Problem Statement

I really dislike the middle name "Hang" because of its undesirable
[connotations](https://en.wikipedia.org/wiki/Hanging) in English, even
though it does not have the same connotations in Chinese.

At the same time, it is perfectly acceptable for someone to use their
middle name in place of their first name, even professionally, as if it
was their real name. To me, this is a double standard. Other people
have the opportunity to use their middle name as an additional name in
place of their first name, but I don't, as stated above.

On the other hand, there can sometimes be a presumption of bad faith whenever
someone uses a name other than their own. Criminals use fake aliases to conceal
their identity, and some people don't buy into the idea of using a name
or alias that doesn't correspond to their own name for professional
purposes, even if other people may have previously used it.

To me, this places me into a sort of dilemma. I have an undesirable middle
name, but at the same time, it's difficult for me to use a different one.

The problem statement can be summarized as follows:

- I want to be able to use names other than "Peter" or "Hang."
- I want to be able to use those names effectively in various situations,
including very serious ones.
- I do not want it to sound fake or actually be fake. In particular, I have to
consider:
  - If I use a name other than my own and don't provide a prior-accepted
  reason, then they may not necessarily want to use it.
  - Using a name other than my own could be viewed as some as concealing
  my identity. I have no intention to do so in many cases.
- I also have to consider:
  - There are still people in my life who are unaware of this Aliases 3000
  system and this can confuse people who still know me as Peter.
  - I may not be satisified with my new names/aliases, so I want to be able
  to use new aliases and abandon old ones should circumstances change, but
  people would still associate them with me.

# Modus operandi

After doing some research on the possible ways to effectively express
those aliases, the best way to meet all of the above bullet points is to
come up with a solution of my own:

1. I have set up a mail server on particular subdomains of peterjin.org.
Email was the best choice because it is nearly universal and is somewhat
more private than social media or other things.
2. I devise an effective system for selecting appropriate names/aliases
like "Charles" or "Elizabeth." How I may choose to implement such an
"effective system" is a work in progress, and currently, it is limited to
first names used only on their own without a surname.
3. I test out those names by creating email accounts with those names and
messaging my friends, either directly through their own email addresses or via
an [SMS gateway](https://en.wikipedia.org/wiki/SMS_gateway). Again, how I may
actually express those in email addresses is also a work in progress.
4. It is known that some people like the idea of using a different name and
some are simply disgusted by it. I have no way of knowing this in advance other
than doing test runs \(which may not be appropriate in all cases), so
I'll try to have a feedback system to allow people to comment on my names,
and this may influence how I select names in the future.
5. The use of those names/aliases is not limited to email and can take the
form of many conveyances, including those in real life.
6. I will publish any names I have widely used, including their conveyances,
on a web app, so people can reasonably attribute my aliases to me and not
believe that they are different identities.
7. In the future, there may be some automation involved to make this system
much easier.

The above is only a very rough outline. The system is constantly in development
and may undergo rigorous changes over time. The latest changes are hosted on a
web app located at [aliases3k.peterjin.org](https://aliases3k.peterjin.org/).

With that being said, if you ever get an email from "Charles" or "Elizabeth"
from my domain, just remember that it's simply part of the system above.
