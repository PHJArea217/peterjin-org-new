2019-06-30|11|-|-|Replacing apps-vm2 with apps-vm3

Back when I had apps-vm2, I had no idea how to operate an Internet server. The
only Internet server I had at that time was something I hosted privately at
home, but it was not public. I had to learn about some of the things one might
experience when setting up an Internet server, like log auditing, high
availability, and protection against attacks.

Apps-vm2 used to be fine, until I started to worry about the possiblity of
not using best practices at the start. Sure, the SSH server was public-key
authentication only and the web server started off as SSL only, and I always
updated the software regularly, but still, there were other things. For example,
I ended up configuring two "primary" mail servers instead of one "primary" and
one "backup", which meant that I had to set up POP3 retrieval twice, and the use
of IMAP was virtually impossible. Because of those past errors, I decided that
it would probably be better to create a new VPS rather than to continue
operating my old VPS.

Enter apps-vm3. This VPS is hosted on Linode, which just as with apps-vm1 and
the previous apps-vm2, offers both IPv4 and IPv6. As of this writing, it is
all set up as a replacement for apps-vm2 \(which does not exist anymore). I
wanted the transition to be seamless, but I did make some major blunders:

* To transition the mail service, I removed the MX record pointing to
```apps-vm2``` on ```neon.peterjin.org``` from the DNS prior to decommissioning
```apps-vm2```, leaving only ```apps-vm1```. However, at the same time, I
wanted to change ```apps-vm1``` to ```apps-vm1.srv```. I placed the new A and
AAAA records of ```apps-vm1.srv```, pointing to the same IP addresses of
```apps-vm1```, then replaced the old A and AAAA records of ```apps-vm1``` with
a CNAME pointing to ```apps-vm1.srv```. However, what I didn't know was that
the mail host part of a MX record should never point to a CNAME record, or
otherwise some mail providers will be unable to send mail to that domain. Given
that it was the only MX record for about 2 1/2 hours, this meant a somewhat
long service disruption for the ```neon.peterjin.org``` mail server from 17:00
to 19:30 UTC \(12:00 pm to 2:30 pm CDT) on Tuesday, 25 June 2019, but only if
your email service does not accept MX pointing to CNAMEs. Given that MTAs are
permitted to not follow CNAMES for MX hosts, this could potentially affect
some senders \(but not others). This only affected ```neon.peterjin.org```; the
main mail server on ```peterjin.org``` was unaffected.
* The ```neon.peterjin.org``` website was unavailable from 15:00 UTC \(10:00 am
CDT) on Tuesday, 25 June 2019 to around 15:00 UTC \(10:00 am CDT) on Wednesday,
26 June 2019. This only affected the website, the email service only had the
above outage. Given that there is not really much on those subdomains other
than the MTA-STS policy, it is probably not that big of a problem.
* The ```go.peterjin.org``` URL shortener, including all other apps hosted
on ```apps-vm2```, was unavailable from 15:00 UTC \(
10:00 am CDT) on Tuesday, 25 June 2019 to around 20:00 UTC \(3:00 pm CDT) the
same day. I had totally forgotten about that, so I migrated all those apps
to ```apps-vm1```. Fortunately, I did it in time.
* The next problem is somewhat embarrassing: If accessed via plain text HTTP,
it is possible for any website which points to ```apps-vm3``` to give a 400 Bad
Request error. This might give off the impression that my website is completely
down. Embarrassingly, it ran all the way from Tuesday, 25 June 2019 to Sunday,
30 June 2019, but that was completely unnoticed due to the use of "HSTS
preloading", which means that I did not notice that the "redirect" from typing
```peterjin.org``` without "https" was because of the browser, not because of
nginx.

But at least I won't make the same mistakes again.
