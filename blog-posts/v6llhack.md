2019-03-24|9|-|-|Harnessing the power of the IPv6 link local address

IPv6 link local addresses have significant advantages, especially on
single-link LANs. They work even without router advertisements or DHCPv6,
even on IPv4-only networks, and even if the global IPv6 address is
misconfigured. They are also immune to renumbering \(in either IPv4 or IPv6),
and on many systems, they are static and don't change over time.
However, if you have ever used IPv6 link local addresses (i.e., the ones
starting with fe80::) for LAN use, especially on Linux, you might have been
irritated by the fact that the addresses have to be supplemented by a "scope
ID", which is usually the name of the output network interface, and some
programs will not allow you to supply it, complaining about the IP address
being invalid.

Fortunately, I discovered a trick with ip6tables that lets you effectively use
those addresses without supplying a scope ID, by mapping an arbitrary
unused global IPv6 address to an actual link-local address and scope ID.

Here are the steps:

1. Find an IPv6 address in an unused region of the IPv6 address space. Doesn't
need to be an entire /64, a single IPv6 address in any region reserved for a
special purpose would be fine. For this demonstration we will use ::150.

2. Determine the link-local address and network interface name of the host
you want to connect to. For our purposes we will use fe80::1 and eth0.

3. Run the following commands on the system that you are connecting from:

```ip6tables -t nat -A OUTPUT -d ::150/128 -j DNAT --to-destination fe80::1```

```route -6 add ::150/128 gw fe80::1 dev eth0```

You should replace ```fe80::1```, ```eth0```, and ```::150``` with your own
link-local and mapped IP addresses.

Both commands are required. The first tells the kernel/netfilter system
to redirect any packets that would go to the IPv6 address ::150 to fe80::1
instead. The second command actually configures the routing, and
despite the destination host being specified as the gateway, the destination
host does not actually need to be a gateway; it is only to ensure that the
IPv6 neighbor discovery performs the lookup on fe80::1 rather than ::150,
and to actually specify the destination interface.

Normally they would be specified in reverse order, but if they were,
then the ::150 IP address would leak in between the two commands.

Now, you can open up a web browser and browse to [::150] \(provided
you actually have a web server on there), and it should work right as
expected.

And, you could even edit your /etc/hosts file to map a local hostname to
```::150``` and access it just like you would with an IPv4 address in your
hosts file.

The only limitation with this is that you can only map single individual
addresses at a time \(and not an entire subnet), but you can certainly
run the above two commands multiple times. If you want to map the entire
IPv6 link-local space, you can do this for the first command \(untested):

```ip6tables -t nat -A OUTPUT -d 2001:db8:1:1::/64 -j DNAT --to-destination fe80::/64```

There are even more cool things with this:

* The mapped addresses don't even need to be the same on every system on
the network \(but obviously the target link-local addresses do). One system
can map \[::150], and another can map \[::2a0] to the same host, etc.

**UPDATE 2019-04-20:** The correct command to do the above is
```ip6tables -t nat -A OUTPUT -d 2001:db8:1:1::/64 -j NETMAP --to fe80::/64```.
If you would like to connect to ```fe80::5:712```, you would use
```2001:db8:1:1::5:712``` to connect to it. The second command \(the one with
"route" in it) may or may not be required, but you can also try
```route -6 add 2001:db8:1:1::/64 dev eth0```. This has only been tested on the
newest Ubuntu 18.04 kernel 4.15.0-47-generic.
