2019-03-24|10|-|-|Redesigning peterjin.org

If you have visited my website prior to 20 March 2019, you might have noticed
that I redesigned this website. But there is much more to it than just
redesigning it.

# The website itself

Previously, this website was a very plain HTML website and wasn't really flashy
at all. Now, we use a generic Bootstrap theme, which gives us a much cleaner
look and a lot of CSS options.

One of the other things I did was move some of the "About" content to a
different location - namely [about.peterjin.org](https://about.peterjin.org),
which allows me to separate the informational content, which has somewhat less
of an appeal compared to the rest of the content.

# Servers

I have recently moved this website from GitLab Pages with Cloudflare CDN to my
own VPS servers. The reason is a bit unusual, since most people \(including me)
would prefer the former, but here it is: Cloudflare's "Universal SSL" uses an
ECC \(ECDSA) certificate, and while I do like this idea in itself \[n1], the
only problem with this is that there is no RSA fallback, which means that any
client that doesn't support ECC will not be able to connect to my website \(as
opposed to presenting an SSL error, which I think is much better than not being
able to connect to my website at all).

My VPS's support a wide variety of cipher suites - TLS 1.0, 1.1, 1.2 \[n2],
ECDHE with ECC/RSA, DHE, static RSA, AES-128/256 CBC/GCM with SHA1 or SHA256,
Cameilla, and ChaCha20 - essentially any combination of modern cipher suites
that most of us would still consider secure \(so no RC4 or \[3]DES); there is
also no SNI requirement, so some even older clients can connect too. But many
other hosting providers don't provide this flexibility, so sometimes my choices
are limited; there's an IPv6 and DNSSEC requirement as well, which makes my
choices even more limited -- see Appendix A for a list of shortcomings.

I understand that some of these choices are not ideal for more secure websites
\(for example, the latest PCI DSS standard prohibits TLS 1.0), but I also
understand that with Let's Encrypt providing SSL to many websites for free,
some early websites that previously did not have SSL still serve legacy clients
and in many cases, switching to SSL can cause those legacy clients to break.
Consequently, in my opinion, it is acceptable for a general purpose blog or
personal website to have many SSL/TLS choices available, but for a more secure
website like one with passwords and credit card information, it may be
mandatory to enforce certain security features like forward secrecy and 256-bit
cipher strength.

Using a VPS for website hosting provides some flexibility in this regard, but
doing so places a bit of bandwidth on my servers, especially considering that
the bus routes app has large JSON files. To work around this, I have considered
the following:

* Design the site like normal, but place a copy of the entire website on a CDN.
* On the website, add a script that checks for a proper connection to the CDN.
If it succeeds, rewrite the location of all &lt;script&gt;, &lt;img&gt;, and
&lt;link&gt; tags to use the CDN.

The idea is that if a web browser is able to connect to the CDN, then it shall
fetch the necessary files from the CDN. Otherwise, serve a local copy. This
allows us to use the CDN to save bandwidth but also provide compatibility to
clients that don't support the CDN.

As it currently stands, SNI \(Server Name Indication) is required, and the main
sites use only an RSA certificate. However, this is only temporary; once the
existing RSA/ECDSA certificates on the VPSs expire, the hostnames will be added
to all of the certificates on the VPSs, allowing ECDSA/RSA combination
certificates with no SNI requirement.

We continue to use GitLab CI for building the website and the ```gitlab.io```
subdomain for testing purposes, but other than that, we've moved hosting to
our own VPSs.

# Load Balancing

Along with switching to a VPS, I have implemented a DNS-based load balancer
that allows me to control traffic flow to one or both VPS's. The mode of
operation is rather simple:

* Normally, traffic is directed to both VPS's to equalize bandwidth.
* If I need to do any maintenance work on one of the VPSs \(for example,
renewing an SSL certificate, testing an experimental configuration,
rebooting/updating the system, etc.), I simply control the DNS to point to the
other VPS.
* This gives us the advantage of high availability and is highly invisible to
visitors of my website, while having the freedom to perform maintenance
operations at will.

# Appendix A: Shortcomings of existing hosting services I've used

This is not to point fingers, but to make a note of the technical limitations
of certain web hosting providers I've used and consider them when deploying
a new subdomain.

* Netlify: IPv6 supported, but TLS 1.2 only.
* Firebase: No IPv6 supported, TLS 1.2 only.
* Google App Engine: IPv6 supported, TLS 1.0 supported, no ECDSA required
* Cloudflare: IPv6 supported, TLS 1.0 supported, ECDSA support required

These are not necessarily relevant when using modern browsers, but they might
be if I plan to support old browsers on those sites.

\[n1] I express no opinion on whether the NSA has inserted backdoors in some
of the elliptic curves used by the protocol like the common prime-256 curve.
The fact that elliptic curve keys are smaller than RSA keys for an equivalent
security level is a sufficient selling point for me.

\[n2] TLS 1.3 is not yet supported, but it is still on my radar.