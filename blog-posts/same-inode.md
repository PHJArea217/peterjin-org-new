2019-06-30|16|-|-|Creating two files with the same device and inode number on Linux

On Linux, it is obviously possible to create two files with the same device and
inode number by way of a hard link or bind mount, however, this would also mean
that the files themselves would still be identical.

To my surprise, I discovered an interesting exception. It is possible to create
two System V shared memory or semaphore objects that basically have the same
inode and device number, yet have completely different contents. Here is how
to do it:

1. Make sure PulseAudio is not running. If it is, run ```pulseaudio -k``` or
```killall pulseaudio```.
2. Open up a root terminal.
3. ```unshare -i aplay -D plug:dmix /dev/zero &```. This will print out a
\[1] followed by a PID number.
4. ```ls -l /proc/[same pid as in step 3]/map_files | grep SYSV```. Look at
the files that point to /SYSV0056a4d5 or /SYSV0056a4d6.
5. Run ```exec 3</proc/[same pid]/map_files/[some address range]```, where
\[some address range] refers to the same file names in step 4. Do this for
each file, but replace the 3&lt; with 4&lt; for the other file.
6. Run ```kill [same pid]```.
7. Repeat steps 3-6 for another instance, but use 5&lt; and 6&lt; for step 5.
8. Run ```echo First file >/dev/fd/3``` and then ```echo Second file >/dev/fd/5```.
9. Run ```stat -Lc %d:%i /proc/self/fd/3```, and do this for fd/4, fd/5, and fd/6.
10. Run ```ls -Lli /proc/self/fd```.
11. Run ```cat /proc/self/fd/3```, and also repeat this for 4/5/6.
12. Notice something strange?

![Two different files with identical device/inode numbers](../misc/2019-06-17-073313_442x181_scrot.png)

That's right: two files, identical device numbers, identical inode numbers,
but completely different contents.
