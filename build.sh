#!/bin/sh
# Arg 1: name of output file
# 2: title (human-readable string)
# 3: navbar script
# 4: actual script to render html content
set -eu

_insert_bootstrap_files() {
	cat <<EOF
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
EOF
}

printf '%s\0' "$1" | sh -c 'exec mkdir -p "$(xargs -0 dirname)"'

UPDIR_TO_ROOT="$(printf '%s' $1 | tr -cd / | wc -c | xargs seq 2 | while read x; do printf '../'; done)"

exec > "$1"

cat <<EOF
<!DOCTYPE html>
<html class="h-100"><head>
<link rel="canonical" href="https://www.peterjin.org/${1##public/}" />
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, shrink-to-fit=no">
<script src="${UPDIR_TO_ROOT}url-rewriter.js" type="text/javascript"></script>
<title>$2 - www.peterjin.org</title>
EOF
[ -f "${HEAD_ADDITIONAL:-}" ] && . "$HEAD_ADDITIONAL"
_insert_bootstrap_files
cat <<EOF
<link rel="stylesheet" href="${UPDIR_TO_ROOT}style.css" />
</head>
<body class="h-100 d-flex flex-column" onload="_rewrite_test();">
EOF

: ${SITE_TITLE:=Peter H. Jin}
NAVBAR_SCRIPT="${3:-navbar-default}"
if [ -k "$NAVBAR_SCRIPT" ]; then
	cat <<EOF
<header><nav class="navbar navbar-expand-md navbar-dark bg-dark">
<a class="navbar-brand" href="$UPDIR_TO_ROOT.">$SITE_TITLE</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-top" aria-controls="navbar-top" aria-expanded="false">
<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbar-top">
EOF
	. "./$NAVBAR_SCRIPT"
	echo '</div></nav></header>'
fi
. "./$4"
if ! [ "1" = "${OMIT_FOOTER:=0}" ]; then
	cat <<EOF
<footer class="footer align-items-end py-2 mt-auto" style="background-color: #ddd;">
<div class="container">
<div class="row" style="text-align: center">
<div class="col-md">Copyright &copy; 2019 Peter H. Jin - <a rewrite-class="about" href="https://about.peterjin.org/">About</a> - <a href="https://serverstatus.apps-1.peterjin.org">Status</a></div>
<div class="col-md">Last updated $(date -u +'%F %T') (<abbr title="Coordinated Universal Time">UTC</abbr>)</div>
</div>
</div>
</footer>
EOF
fi
cat <<EOF
<div id="cookie-disclaimer" class="container" style="background-color: #eff; bottom: 0; right: 0; left: 0; position: fixed; width: 100%; height: 60px; font-size: 12px; display: none;">
This site uses cookies to enhance your browsing experience. By using this site, you consent to our use of cookies.
<a href="javascript:document.getElementById('cookie-disclaimer').style.display = 'none';document.cookie = 'acceptCookies=1;';void(0);">Close</a>
</div>
<script>if (String(document.cookie).indexOf('acceptCookies=1') == -1) document.getElementById('cookie-disclaimer').style.display = 'block';</script>
</body></html>
EOF
