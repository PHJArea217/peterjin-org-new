all: public/index.html public/blog/index.html
public/index.html: build.sh index.script
	sh build.sh $@ "Official Website of Peter H. Jin" '' index.script
public/blog/index.html: build.sh blog.script
	env IS_BLOG_SUBDIR=. sh build.sh $@ "peterjin.org blog" '' blog.script
